#include <SFML/Graphics.hpp>
using namespace sf;

//######################
//##### CONSTANTES #####
//######################

#define FENETRE_LARGEUR 1400
#define FENETRE_HAUTEUR 700

//////////////////////////////////
//D�but de la fonction principale/
//////////////////////////////////
int main()
{
   RenderWindow fenetre(VideoMode(FENETRE_LARGEUR, FENETRE_HAUTEUR), "PROJET SUPER GENIAL");

/////////////////
//Image du ciel//
/////////////////
    Texture ciel;

    if (!ciel.loadFromFile("ciel.png"))
        printf("Le chargement de l'image ne s'est pas effectue\n");

    Sprite imageCiel;
    imageCiel.setTexture(ciel);



//#######################
//##### AFFICHAGE #######
//#######################
    fenetre.draw(imageCiel);
    fenetre.display();
    system("pause");

    return 0;
}
